#include "common.h"


bool Mesh::loadOBJ(const char * path){
  std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
  std::vector< vec3 > temp_vertices;
  std::vector< vec2 > temp_uvs;
  std::vector< vec3 > temp_normals;
  
  hasUV = true;
  
  FILE * file = fopen(path, "r");
  if( file == NULL ){
    printf("Impossible to open the file !\n");
    return false;
  }
  
  char *line = new char[128];
  char *lineHeader = new char[128];
  
  while(true){
    memset(line, 0 , 128);
    memset(lineHeader, 0 , 128);
    
    if(fgets(line, 128, file) == NULL){ break; }
    sscanf(line, "%s ", lineHeader);
    
    if ( strcmp( lineHeader, "v" ) == 0 ){
      vec3 vertex;
      sscanf(&line[2], "%f %f %f", &vertex.x, &vertex.y, &vertex.z );
      temp_vertices.push_back(vertex);
      if(vertex.x < box_min.x){box_min.x = vertex.x; }
      if(vertex.y < box_min.y){box_min.y = vertex.y; }
      if(vertex.z < box_min.z){box_min.z = vertex.z; }
      if(vertex.x > box_max.x){box_max.x = vertex.x; }
      if(vertex.y > box_max.y){box_max.y = vertex.y; }
      if(vertex.z > box_max.z){box_max.z = vertex.z; }
    }else if ( strcmp( lineHeader, "vt" ) == 0 ){
      vec2 uv;
      sscanf(&line[3], "%f %f", &uv.x, &uv.y );
      temp_uvs.push_back(uv);
    }else if ( strcmp( lineHeader, "vn" ) == 0 ){
      vec3 normal;
      sscanf(&line[3], "%f %f %f", &normal.x, &normal.y, &normal.z );
      temp_normals.push_back(normal);
    }else if ( strcmp( lineHeader, "f" ) == 0 ){
      std::string vertex1, vertex2, vertex3;
      int vertexIndex[3], uvIndex[3], normalIndex[3];
      int matches = sscanf(&line[2], "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0],
                           &vertexIndex[1], &uvIndex[1], &normalIndex[1],
                           &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
      if (matches != 9){
        int matches = sscanf(&line[2], "%d//%d %d//%d %d//%d\n", &vertexIndex[0], &normalIndex[0],
                             &vertexIndex[1], &normalIndex[1],
                             &vertexIndex[2], &normalIndex[2]);
        if (matches == 6){
          hasUV = false;
        } else {
          printf("File can't be read by our simple parser : ( Try exporting with other options\n");
          return false;
        }
      }
      
      /* handle negative indices */
      /* (adjust for size during processing of each face, as per the old
       *  OBJ specification, instead of after the end of the file) */
      for (int negati = 0; negati < 3; negati++){
        if (vertexIndex[negati] < 0){
          vertexIndex[negati]+=temp_vertices.size();
          vertexIndex[negati]++; /* <- OBJ indices are one-based */
        }
        if (uvIndex[negati] < 0){
          uvIndex[negati]+=temp_uvs.size();
          uvIndex[negati]++;
        }
        if (normalIndex[negati] < 0){
          normalIndex[negati]+=temp_normals.size();
          normalIndex[negati]++;
        }
      }
      
      vertexIndices.push_back(vertexIndex[0]);
      vertexIndices.push_back(vertexIndex[1]);
      vertexIndices.push_back(vertexIndex[2]);
      if(hasUV){
        uvIndices    .push_back(uvIndex[0]);
        uvIndices    .push_back(uvIndex[1]);
        uvIndices    .push_back(uvIndex[2]);
      }
      normalIndices.push_back(normalIndex[0]);
      normalIndices.push_back(normalIndex[1]);
      normalIndices.push_back(normalIndex[2]);
    }
  }
  
  delete[] line;
  delete[] lineHeader;
  
  // For each vertex of each triangle
  for( unsigned int i=0; i<vertexIndices.size(); i++ ){
    unsigned int vertexIndex = vertexIndices[i];
    vec4 vertex = vec4(temp_vertices[ vertexIndex-1 ], 1.0);
    vertices.push_back(vertex);
  }
  
  if(hasUV){
    for( unsigned int i=0; i<uvIndices.size(); i++ ){
      unsigned int uvIndex = uvIndices[i];
      vec2 uv = temp_uvs[ uvIndex-1 ];
      uvs.push_back(uv);
    }
  }
  
  for( unsigned int i=0; i<normalIndices.size(); i++ ){
    unsigned int normalIndex = normalIndices[i];
    vec3 normal = temp_normals[ normalIndex-1 ];
    normals.push_back(normal);
  }
    
  center = box_min+(box_max-box_min)/2.0;
  scale = (std::max)(box_max.x - box_min.x, box_max.y-box_min.y);
  
  model_view = Scale(1.0/scale,           //Make the extents 0-1
                     1.0/scale,
                     1.0/scale)*
  Translate(-center);  //Orient Model About Center
  
  
  return true;
}

bool Mesh::makeSphere(int steps){
  //TODO:  FILL IN vertices, normals, and uv vectors
    float theta1;    //longitude running from 0 to 2*pi
    float theta2;
    float phi1;      //latitude running from 0 to pi.
    float phi2;
    float u;
    float v;
    vec4 p1t1;
    vec4 p1t2;
    vec4 p2t1;
    vec4 p2t2;
    vec3 normal;
	int z;
    for (unsigned int i = 0; i < steps; ++i) {
        phi1 = i * M_PI / (steps);
        phi2 = (i + 1) * M_PI / (steps);
        
		

		for (unsigned int j = 0; j < steps; ++j) {

            theta1 = j * (2 * M_PI) / (steps);
            theta2 = (j + 1) * (2 * M_PI) / (steps);
            //first triangle
            p1t1 = vec4(cos(theta1) * sin(phi1), sin(theta1) * sin(phi1), cos(phi1), 1.0);
            normal = normalize(vec3(p1t1.x, p1t1.y, p1t1.z));
            //u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
            //u = 0.5 + (asin(normal.x) / M_PI);
            u = theta1 / (2 * M_PI);
            v = phi1 / M_PI;
            vertices.push_back(p1t1);
            normals.push_back(normal);
            uvs.push_back(vec2(u, v));
            p1t2 = vec4(cos(theta2) * sin(phi1), sin(theta2) * sin(phi1), cos(phi1), 1.0);
            normal = normalize(vec3(p1t2.x, p1t2.y, p1t2.z));
            u = theta2 / (2 * M_PI);
            v = phi1 / M_PI;
            vertices.push_back(p1t2);
            normals.push_back(normal);
            uvs.push_back(vec2(u, v));
            p2t1 = vec4(cos(theta1) * sin(phi2), sin(theta1) * sin(phi2), cos(phi2), 1.0);
            normal = normalize(vec3(p2t1.x, p2t1.y, p2t1.z));
            u = theta1 / (2 * M_PI);
            v = phi2 / M_PI;
            vertices.push_back(p2t1);
            normals.push_back(normal);
            uvs.push_back(vec2(u, v));
            
            //second triangle
            p1t2 = vec4(cos(theta2) * sin(phi1), sin(theta2) * sin(phi1), cos(phi1), 1.0);
            normal = normalize(vec3(p1t2.x, p1t2.y, p1t2.z));
            u = theta2 / (2 * M_PI);
            v = phi1 / M_PI;
            vertices.push_back(p1t2);
            normals.push_back(normal);
            uvs.push_back(vec2(u, v));
            p2t1 = vec4(cos(theta1) * sin(phi2), sin(theta1) * sin(phi2), cos(phi2), 1.0);
            normal = normalize(vec3(p2t1.x, p2t1.y, p2t1.z));
            u = theta1 / (2 * M_PI);
            v = phi2 / M_PI;
            vertices.push_back(p2t1);
            normals.push_back(normal);
            uvs.push_back(vec2(u, v));
            p2t2 = vec4(cos(theta2) * sin(phi2), sin(theta2) * sin(phi2), cos(phi2), 1.0);
            normal = normalize(vec3(p2t2.x, p2t2.y, p2t2.z));
            u = theta2 / (2 * M_PI);
            v = phi2 / M_PI;
            vertices.push_back(p2t2);
            normals.push_back(normal);
            uvs.push_back(vec2(u, v));

			/*
			if (j == (steps - 1)) {

				//phi1 = (i + 1) * M_PI / (steps);
				//phi2 = 0;

				theta1 = (j + 1) * (2 * M_PI) / (steps);
				theta2 = 0;
				//first triangle
				p1t1 = vec4(cos(theta1) * sin(phi1), sin(theta1) * sin(phi1), cos(phi1), 1.0);
				normal = normalize(vec3(p1t1.x, p1t1.y, p1t1.z));
				//u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				//u = 0.5 + (asin(normal.x) / M_PI);
				u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				v = 1.0;
				vertices.push_back(p1t1);
				normals.push_back(normal);
				uvs.push_back(vec2(u, v));
				p1t2 = vec4(cos(theta2) * sin(phi1), sin(theta2) * sin(phi1), cos(phi1), 1.0);
				normal = normalize(vec3(p1t2.x, p1t2.y, p1t2.z));
				u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				v = 0.0;
				vertices.push_back(p1t2);
				normals.push_back(normal);
				uvs.push_back(vec2(u, v));
				p2t1 = vec4(cos(theta1) * sin(phi2), sin(theta1) * sin(phi2), cos(phi2), 1.0);
				normal = normalize(vec3(p2t1.x, p2t1.y, p2t1.z));
				u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				v = 1.0;
				vertices.push_back(p2t1);
				normals.push_back(normal);
				uvs.push_back(vec2(u, v));

				//second triangle
				p1t2 = vec4(cos(theta2) * sin(phi1), sin(theta2) * sin(phi1), cos(phi1), 1.0);
				normal = normalize(vec3(p1t2.x, p1t2.y, p1t2.z));
				u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				v = 0.0;
				vertices.push_back(p1t2);
				normals.push_back(normal);
				uvs.push_back(vec2(u, v));
				p2t1 = vec4(cos(theta1) * sin(phi2), sin(theta1) * sin(phi2), cos(phi2), 1.0);
				normal = normalize(vec3(p2t1.x, p2t1.y, p2t1.z));
				u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				v = 1.0;
				vertices.push_back(p2t1);
				normals.push_back(normal);
				uvs.push_back(vec2(u, v));
				p2t2 = vec4(cos(theta2) * sin(phi2), sin(theta2) * sin(phi2), cos(phi2), 1.0);
				normal = normalize(vec3(p2t2.x, p2t2.y, p2t2.z));
				u = 0.5 + (atan2(normal.z, normal.x) / (2 * M_PI));
				v = 0.0;
				vertices.push_back(p2t2);
				normals.push_back(normal);
				uvs.push_back(vec2(u, v));
			}
			*/
			
        }

		
    }
    
  return true;
}

bool Mesh::makeParametricSphere(int steps){
    
	return true;
}
